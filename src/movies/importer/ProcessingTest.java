/* Grigor  Mihaylov */
package movies.importer;

import java.io.IOException;

public class ProcessingTest {
	public static void main(String[] args) throws IOException{
		String dirSrc = "D:\\MyCode\\Java\\fall2020lab05_FILES_FOLDER";
		String dirDestinationLower =  "D:\\MyCode\\Java\\fall2020lab05_FILES_FOLDER\\DistanationForLowerCase";
		String dirDestinationRemove = "D:\\MyCode\\Java\\fall2020lab05_FILES_FOLDER\\DestinationForRemoveDups";
		
		LowercaseProcessor lowerCasesApp = new LowercaseProcessor(dirSrc,dirDestinationLower);
		lowerCasesApp.execute();
		
		RemoveDuplicates removeDupsApp = new RemoveDuplicates(dirDestinationLower,dirDestinationRemove);
		removeDupsApp.execute();
		
	}
}
