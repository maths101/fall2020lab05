/* Grigor  Mihaylov */
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> reduced = new ArrayList<String>();
		for ( String s: input) {
			if(!reduced.contains(s)) {
				reduced.add(s);
			}
		}
		return reduced;
	}

}
